class AddGaCodeToSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :ga_code, :string
  end
end
