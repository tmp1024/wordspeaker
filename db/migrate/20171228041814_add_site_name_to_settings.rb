class AddSiteNameToSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :site_name, :string, null: false, default: ''
  end
end
