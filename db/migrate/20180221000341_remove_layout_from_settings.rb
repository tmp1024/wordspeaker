class RemoveLayoutFromSettings < ActiveRecord::Migration[5.1]
  def change
    remove_column :settings, :layout, :string
  end
end
