class AddGoogleSiteVerificationToSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :google_site_verification, :string
  end
end
