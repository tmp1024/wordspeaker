class CreateArticleDrafts < ActiveRecord::Migration[5.1]
  def change
    create_table :article_drafts do |t|
      t.string :title, null: false
      t.text :body
      t.integer :document_type, null: false, default: 0
      t.string :permalink, null: false, default: ''
      t.references :article, foreign_key: true

      t.timestamps
    end
  end
end
