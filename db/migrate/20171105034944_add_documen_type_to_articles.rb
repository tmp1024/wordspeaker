class AddDocumenTypeToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :document_type, :integer, default: 0, null: false
  end
end
