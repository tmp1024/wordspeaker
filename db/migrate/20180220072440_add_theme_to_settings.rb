class AddThemeToSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :theme, :string, null: false, default: 'default'
  end
end
