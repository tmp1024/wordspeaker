class AddSyntaxHighlightToSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :syntax_highlight, :integer, null: false, default: 0
  end
end
