class CreateSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :settings do |t|
      t.string :layout, null: false, default: 'default'

      t.timestamps
    end
  end
end
