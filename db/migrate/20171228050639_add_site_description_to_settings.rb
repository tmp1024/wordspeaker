class AddSiteDescriptionToSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :site_description, :text
  end
end
