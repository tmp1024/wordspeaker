Rails.application.routes.draw do

  scope :init, as: :init do
    get 'index', to: 'initial_settings#index'
    get 'setting', to: 'initial_settings#setting'
    post 'setting', to: 'initial_settings#create_setting'
    get 'completion', to: 'initial_settings#completion'
  end

  namespace :admin do
    get 'settings/index'
  end

  authenticated :user do
    namespace :admin do
      resources :articles do
        collection do
          post 'preview'
        end
      end
      resources :categories
      resources :tags
      resources :images

      get '/', to: 'dashboard#index'
      get 'settings', to: 'settings#index'
      patch 'settings', to: 'settings#update'

      # resource :users, only: %i[index edit update]
      authenticated :user, lambda { |u| u.admin? } do
        get 'users', to: 'users#index'
        get 'users/index'
      end
    end
  end

  resources :tags, only: %i[index show]
  resources :categories, only: %i[index show]
  resources :articles, param: :permalink, only: %i[index show]

  devise_for :users, path: 'admin/users', controllers: {
    registrations: 'users/registrations',
    passwords: 'users/passwords',
    sessions: 'users/sessions'
  }

  devise_scope :user do
    get 'admin/users/:id/edit', to: 'users/registrations#other_edit', as: :edit_admin_user
    put 'admin/users/:id', to: 'users/registrations#other_update', as: :admin_user
    delete 'admin/users/:id', to: 'users/registrations#other_destroy'
  end

  root 'articles#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
