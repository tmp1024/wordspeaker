namespace :sitemap do
  task generate: :environment do
    SitemapGenerator.verbose = false

    SitemapGenerator::Sitemap.default_host = Settings.site[:url]

    SitemapGenerator::Sitemap.create do
      Article.find_each do |article|
        add article_path(article.permalink), lastmod: article.updated_at if article.publication?
      end
    end

    SitemapGenerator::Sitemap.ping_search_engines if Rails.env.production?
  end
end
