namespace :theme do
  task create_symlink: :environment do
    if Setting.count.zero?
      setting = Setting.new
      setting.theme = 'default'
    else
      setting = Setting.first
    end
    setting.create_theme_symlink
  end

  task copy: :environment do
    if Setting.count.zero?
      setting = Setting.new
      setting.theme = 'default'
    else
      setting = Setting.first
    end
    setting.copy_theme
  end
end
