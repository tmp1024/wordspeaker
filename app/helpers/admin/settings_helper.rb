module Admin::SettingsHelper
  def theme_directories
    directories = Dir.glob('public/theme/*/')
    directories.map { |dir| File.basename(dir) }
  end
end
