class ArticlesController < ApplicationController
  before_action :set_article, only: %i[show]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.only_publication
  end

  # GET /articles/1
  # GET /articles/1.json
  def show; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find_by!(permalink: params[:permalink], status: Article.statuses[:publication])
  end
end
