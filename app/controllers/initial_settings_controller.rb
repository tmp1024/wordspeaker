class InitialSettingsController < ApplicationController
  skip_before_action :should_not_be_initialize?
  before_action :can_initialize?, except: :completion
  before_action :setting_params, only: :create_setting

  layout 'init'

  def index; end

  def setting
    @setting = Setting.new
  end

  def create_setting
    @setting = Setting.new(setting_params)

    respond_to do |format|
      if @setting.save
        format.html { redirect_to init_completion_url }
      else
        format.html { render :setting }
      end
    end
  end

  def completion; end

  private

  def can_initialize?
    create_initial_user if User.count.zero?
    redirect_to root_url unless Setting.count.zero?
  end

  def setting_params
    params.fetch(:setting, {}).permit(:site_name, :site_description, :syntax_highlight, :theme)
  end
end
