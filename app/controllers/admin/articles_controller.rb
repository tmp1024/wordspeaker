class Admin::ArticlesController < AdminController
  protect_from_forgery except: :preview
  before_action :set_article, only: %i[show edit update destroy]

  # GET /admin/articles
  # GET /admin/articles.json
  def index
    @articles = Article.all
  end

  # GET /admin/articles/1
  # GET /admin/articles/1.json
  def show; end

  def preview
    @article = Article.new(article_params)

    render template: 'articles/show', layout: 'application'
  end

  # GET /admin/articles/new
  def new
    @image = Image.new
    @images = Image.all
    @article = Article.new
  end

  # GET /admin/articles/1/edit
  def edit
    @image = Image.new
    @images = Image.all
  end

  # POST /admin/articles
  # POST /admin/articles.json
  def create
    @article = Article.new(article_params)
    @article.user_ids = [current_user.id]

    respond_to do |format|
      if @article.save
        format.html { redirect_to admin_article_url(@article), notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/articles/1
  # PATCH/PUT /admin/articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to admin_article_url(@article), notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/articles/1
  # DELETE /admin/articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to admin_articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def article_params
    params.fetch(:article, {}).permit(:title, :body, :document_type, :permalink, :status, :publication_date, category_ids: [], tag_ids: [])
  end
end
