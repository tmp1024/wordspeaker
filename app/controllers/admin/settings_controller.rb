class Admin::SettingsController < AdminController
  before_action :set_settings

  def index; end

  def update
    respond_to do |format|
      if @setting.update(setting_params)
        format.html { redirect_to admin_settings_url, notice: 'Setting was succesfully updated.' }
        format.json { render :index, status: :ok, location: @setting }
      else
        format.html { render :index }
        format.json { render json: @setting.errors, statsu: :unprocessable_entity }
      end
    end
  end

  private

  def set_settings
    @setting = Setting.first
  end

  def setting_params
    params.fetch(:setting, {}).permit(:site_name, :site_description, :syntax_highlight, :theme, :ga_code, :google_site_verification)
  end
end
