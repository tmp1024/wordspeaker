class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :should_not_be_initialize?
  before_action :set_setting
  before_action :create_theme_symlink

  def should_not_be_initialize?
    create_initial_user if User.count.zero?
    redirect_to init_index_url if Setting.count.zero?
  end

  def set_setting
    @setting = Setting.first
  end

  def create_initial_user
    User.create(email: 'admin@example.com', password: 'W0rdSpeaker', admin: true)
  end

  def authenticate_admin!
    authenticate_user!
    render file: Rails.root.join('public', '404.html'), layout: false, status: 404 unless current_user.admin?
  end

  def create_theme_symlink
    path = Rails.root.join('app', 'views')
    @setting.create_theme_symlink unless File.exist?("#{path}/articles") && File.exist?("#{path}/categories") && File.exist?("#{path}/tags")
  end
end
