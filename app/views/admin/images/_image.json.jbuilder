json.id @image.id
json.image_url image_url(@image.url)
json.extract! @image, :alt, :title, :created_at, :updated_at
