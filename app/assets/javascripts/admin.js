//= require jquery/dist/jquery.js
//= require popper.js/dist/umd/popper.js
//= require bootstrap/dist/js/bootstrap.js
//= require admin/article.js

$(document).on('turbolinks:load', function () {
    FontAwesome.dom.i2svg();
});
