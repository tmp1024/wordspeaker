$(document).on('click', '.article-preview-button', function () {
    window.open('', 'preview');
    var $form = $(this).closest('form');
    var prevAction = $form.attr('action');
    var prevMethod = $form.attr('method');
    var method = $form.find('input[name="_method"]');
    if (method.length) {
        method.val('post');
    }
    $form.attr({
        action: $(this).data('action'),
        method: 'post',
        target: 'preview'
    });
    $form.submit();

    $form.attr('action', prevAction);
    $form.attr('method', prevMethod);
    if (method.length) {
        method.val('patch');
    }
    $form.removeAttr('target');

    return false;
});

$(document).on('click', '.article-media-button', function () {
    $('.article-media-modal-background').show();
    $('.article-media-modal').show();
});

$(document).on('click', '.modal-remove-icon', function () {
    $('.article-media-modal').hide();
    $('.article-media-modal-background').hide();
});

$(document).on('click', '.modal-image-addition-button', function () {
    $('.modal-image-addition-form').show();
});

$(document).on('click', '#modal-image-submit-button', function (event) {
    var $form = $(this).closest('form');
    var formData = new FormData($form.get()[0]);

    $.ajax({
        url: $form.attr('action'),
        method: 'post',
        dataType: 'json',
        data: formData,
        processData: false,
        contentType: false
    })
    .done(function () {
        $form.find('#image_image').val('');
        $form.find('#image_alt').val('');
        $form.find('#image_title').val('');
        $('.modal-image-addition-form').hide();
    })
    .fail(function () {
        console.log('error');
    });

    return false;
});

$(document).on('click', '.modal-image', function () {
    var imageUrl = $(this).parent().find('input[name="image_url"]').val();
    var imageAlt = $(this).parent().find('input[name="alt"]').val();
    var imageTitle = $(this).parent().find('input[name="title"]').val();
    var html = $('#article_body').val()
            + '\n<img src="'
            + imageUrl
            + '" alt="'
            + imageAlt
            + '" title="'
            + imageTitle
            + '">\n';
    $('#article_body').val(html);

    $('.article-media-modal').hide();
    $('.article-media-modal-background').hide();
});
