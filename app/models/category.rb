class Category < ApplicationRecord
  has_many :category_articles, dependent: :delete_all
  has_many :articles, through: :category_articles

  validates :title, presence: true
end
