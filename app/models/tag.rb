class Tag < ApplicationRecord
  has_many :tag_articles, dependent: :delete_all
  has_many :articles, through: :tag_articles

  validates :title, presence: true
end
