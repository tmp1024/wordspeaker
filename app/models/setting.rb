class Setting < ApplicationRecord
  enum syntax_highlight: { 'base16 light' => 0,
                           'base16 dark' => 1,
                           'colorful' => 2,
                           'github' => 3,
                           'gruvbox light' => 4,
                           'gruvbox dark' => 5,
                           'igor pro' => 6,
                           'molokai' => 7,
                           'monokai' => 8,
                           'monokai sublime' => 9,
                           'thankful eyes' => 10,
                           'tulip' => 11 }

  after_save :create_theme_symlink

  validates :site_name, presence: true
  validates :syntax_highlight, presence: true
  validates :theme, presence: true

  def copy_theme
    source_path = Rails.root.join('public', 'theme', theme, 'views')
    dest_path = Rails.root.join('app', 'views')
    FileUtils.rm_r("#{dest_path}/articles", force: true)
    FileUtils.rm_r("#{dest_path}/categories", force: true)
    FileUtils.rm_r("#{dest_path}/tags", force: true)
    FileUtils.cp_r("#{source_path}/articles", dest_path, remove_destination: true)
    FileUtils.cp_r("#{source_path}/categories", dest_path, remove_destination: true)
    FileUtils.cp_r("#{source_path}/tags", dest_path, remove_destination: true)
  end

  def create_theme_symlink
    source_path = Rails.root.join('public', 'theme', theme, 'views')
    dest_path = Rails.root.join('app', 'views')
    FileUtils.rm_r("#{dest_path}/articles", force: true)
    FileUtils.rm_r("#{dest_path}/categories", force: true)
    FileUtils.rm_r("#{dest_path}/tags", force: true)
    FileUtils.symlink("#{source_path}/articles", dest_path, force: true)
    FileUtils.symlink("#{source_path}/categories", dest_path, force: true)
    FileUtils.symlink("#{source_path}/tags", dest_path, force: true)
  end
end
