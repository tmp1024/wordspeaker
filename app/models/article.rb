class Article < ApplicationRecord
  enum status: { draft: 0, publication: 1 }
  enum document_type: { html: 0, markdown: 1 }

  after_commit :update_sitemap
  after_destroy :update_sitemap

  has_many :article_drafts, dependent: :delete_all
  has_many :category_articles, dependent: :delete_all
  has_many :categories, through: :category_articles
  has_many :tag_articles, dependent: :delete_all
  has_many :tags, through: :tag_articles
  has_many :user_articles, dependent: :delete_all
  has_many :users, through: :user_articles

  validates :title, presence: true
  validates :status,
            presence: true,
            inclusion: { in: Article.statuses.keys }
  validates :document_type,
            presence: true,
            inclusion: { in: Article.document_types.keys }
  validates :permalink,
            presence: true,
            uniqueness: true,
            length: { maximum: 128 },
            format: { with: /\A[A-Za-z0-9\-_]+\z/ }

  validate :check_publication_date_presence

  scope :only_publication, -> { where(status: Article.statuses[:publication]).where('publication_date <= ?', Time.zone.now) }

  private

  def check_publication_date_presence
    errors.add(:publication_date, "can't be blank") if publication? && publication_date.blank?
  end

  def update_sitemap
    Rails.application.load_tasks
    Rake::Task['sitemap:generate'].execute
  end
end
