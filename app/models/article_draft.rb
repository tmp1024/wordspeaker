class ArticleDraft < ApplicationRecord
  enum document_type: { html: 0, markdown: 1 }

  belongs_to :article

  validates :title, presence: true
  validates :document_type,
            presence: true,
            inclusion: { in: Article.document_types.keys }
  validates :permalink,
            presence: true,
            uniqueness: true,
            length: { maximum: 128 },
            format: { with: /\A[A-Za-z0-9\-_]+\z/ }
end
