require 'test_helper'

class Admin::SettingsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @setting = settings(:one)
  end

  test 'should not get index when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_settings_url
    end
  end

  test 'should get index when logged in' do
    sign_in(users(:john))
    get admin_settings_url
    assert_response :success
  end

  test 'should not update setting when not logged in' do
    assert_raises(ActionController::RoutingError) do
      patch admin_settings_url, params: { setting: { site_name: 'wordspeaker', site_description: '', syntax_highlight: 'base16 light', theme: 'default' } }
    end
  end

  test 'should update setting when logged in' do
    sign_in(users(:john))
    patch admin_settings_url, params: { setting: { site_name: 'wordspeaker', site_description: '', syntax_highlight: 'base16 light', theme: 'default' } }
    assert_redirected_to admin_settings_url
  end
end
