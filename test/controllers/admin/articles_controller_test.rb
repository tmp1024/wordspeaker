require 'test_helper'

class Admin::ArticlesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @article = articles(:one)
  end

  test 'should not get index when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_articles_url
    end
  end

  test 'should get index when logged in' do
    sign_in(users(:john))
    get admin_articles_url
    assert_response :success
  end

  test 'should not get new when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get new_admin_article_url
    end
  end

  test 'should get new when logged in' do
    sign_in(users(:john))
    get new_admin_article_url
    assert_response :success
  end

  test 'should not create article when not logged in' do
    assert_raises(ActionController::RoutingError) do
      post admin_articles_url, params: { article: { title: 'New article', permalink: 'new-article' } }
    end
  end

  test 'should create article when logged in' do
    sign_in(users(:john))
    assert_difference('Article.count') do
      post admin_articles_url, params: { article: { title: 'New article', permalink: 'new-article' } }
    end

    assert_redirected_to admin_article_url(Article.last)
  end

  test 'should not show article when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_article_url(@article)
    end
  end

  test 'should show article when logged in' do
    sign_in(users(:john))
    get admin_article_url(@article)
    assert_response :success
  end

  test 'should not preview article when not logged in' do
    assert_raises(ActionController::RoutingError) do
      post preview_admin_articles_url, params: { article: { title: 'New article', permalink: 'new-article' } }
    end
  end

  test 'should preview article when logged in' do
    sign_in(users(:john))
    post preview_admin_articles_url, params: { article: { title: 'New article', permalink: 'new-article' } }
    assert_response :success
  end

  test 'should not get edit when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get edit_admin_article_url(@article)
    end
  end

  test 'should get edit when logged in' do
    sign_in(users(:john))
    get edit_admin_article_url(@article)
    assert_response :success
  end

  test 'should not update article when not logged in' do
    assert_raises(ActionController::RoutingError) do
      patch admin_article_url(@article), params: { article: { title: 'Update article' } }
    end
  end

  test 'should update article when logged in' do
    sign_in(users(:john))
    patch admin_article_url(@article), params: { article: { title: 'Update article' } }
    assert_redirected_to admin_article_url(@article)
  end

  test 'should not destroy article when not logged in' do
    assert_raises(ActionController::RoutingError) do
      delete admin_article_url(@article)
    end
  end

  test 'should destroy article when logged in' do
    sign_in(users(:john))
    assert_difference('Article.count', -1) do
      delete admin_article_url(@article)
    end

    assert_redirected_to admin_articles_url
  end

  test 'should create article associated with categories' do
    sign_in(users(:john))
    category = Category.create(title: 'Test category')
    assert_difference('CategoryArticle.count') do
      post admin_articles_url, params: { article: { title: 'New article', permalink: 'new-article', category_ids: [category.id] } }
    end

    assert_redirected_to admin_article_url(Article.last)
  end

  test 'should update article associated with categories' do
    sign_in(users(:john))
    category = Category.create(title: 'Test category')
    assert_difference('CategoryArticle.count') do
      patch admin_article_url(@article), params: { article: { title: 'Update article', category_ids: @article.category_ids.push(category.id) } }
    end

    assert_redirected_to admin_article_url(@article)
  end

  test 'should create article associated with tags' do
    sign_in(users(:john))
    tag = Tag.create(title: 'Test tag')
    assert_difference('TagArticle.count') do
      post admin_articles_url, params: { article: { title: 'New article', permalink: 'new-article', tag_ids: [tag.id] } }
    end

    assert_redirected_to admin_article_url(Article.last)
  end

  test 'should update article associated with tags' do
    sign_in(users(:john))
    tag = Tag.create(title: 'Test tag')
    assert_difference('TagArticle.count') do
      patch admin_article_url(@article), params: { article: { title: 'Update article', tag_ids: @article.tag_ids.push(tag.id) } }
    end

    assert_redirected_to admin_article_url(@article)
  end
end
