require 'test_helper'

class Admin::CategoriesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @category = categories(:one)
  end

  test 'should not get index when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_categories_url
    end
  end

  test 'should get index when logged in' do
    sign_in(users(:john))
    get admin_categories_url
    assert_response :success
  end

  test 'should not get new when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get new_admin_category_url
    end
  end

  test 'should get new when logged in' do
    sign_in(users(:john))
    get new_admin_category_url
    assert_response :success
  end

  test 'should not create category when not logged in' do
    assert_raises(ActionController::RoutingError) do
      post admin_categories_url, params: { category: { description: @category.description, title: @category.title } }
    end
  end

  test 'should create category when logged in' do
    sign_in(users(:john))
    assert_difference('Category.count') do
      post admin_categories_url, params: { category: { description: @category.description, title: @category.title } }
    end

    assert_redirected_to admin_category_url(Category.last)
  end

  test 'should not show category when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_category_url(@category)
    end
  end

  test 'should show category when logged in' do
    sign_in(users(:john))
    get admin_category_url(@category)
    assert_response :success
  end

  test 'should not get edit when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get edit_admin_category_url(@category)
    end
  end

  test 'should get edit when logged in' do
    sign_in(users(:john))
    get edit_admin_category_url(@category)
    assert_response :success
  end

  test 'should not update category when not logged in' do
    assert_raises(ActionController::RoutingError) do
      patch admin_category_url(@category), params: { category: { description: @category.description, title: @category.title } }
    end
  end

  test 'should update category when logged in' do
    sign_in(users(:john))
    patch admin_category_url(@category), params: { category: { description: @category.description, title: @category.title } }
    assert_redirected_to admin_category_url(@category)
  end

  test 'should not destroy category when not logged in' do
    assert_raises(ActionController::RoutingError) do
      delete admin_category_url(@category)
    end
  end

  test 'should destroy category when logged in' do
    sign_in(users(:john))
    assert_difference('Category.count', -1) do
      delete admin_category_url(@category)
    end

    assert_redirected_to admin_categories_url
  end
end
