require 'test_helper'

class Admin::ImagesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @image = images(:one)
  end

  test 'should not get index when logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_images_url
    end
  end

  test 'should get index when logged in' do
    sign_in(users(:john))
    get admin_images_url
    assert_response :success
  end

  test 'should not get new when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get new_admin_image_url
    end
  end

  test 'should get new when logged in' do
    sign_in(users(:john))
    get new_admin_image_url
    assert_response :success
  end

  test 'should not create image when not logged in' do
    assert_raises(ActionController::RoutingError) do
      post admin_images_url, params: { image: { image: Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'images', 'mountain.jpg'), 'image/jpeg'), alt: @image.alt, title: @image.title } }
    end
  end

  test 'should create image when logged in' do
    sign_in(users(:john))
    assert_difference('Image.count') do
      post admin_images_url, params: { image: { image: Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'images', 'mountain.jpg'), 'image/jpeg'), alt: @image.alt, title: @image.title } }
    end

    assert_redirected_to admin_image_url(Image.last)
  end

  test 'should not show image when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_image_url(@image)
    end
  end

  test 'should show image when logged in' do
    sign_in(users(:john))
    get admin_image_url(@image)
    assert_response :success
  end

  test 'should not get edit when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get edit_admin_image_url(@image)
    end
  end

  test 'should get edit when logged in' do
    sign_in(users(:john))
    get edit_admin_image_url(@image)
    assert_response :success
  end

  test 'should not update image when not logged in' do
    assert_raises(ActionController::RoutingError) do
      patch admin_image_url(@image), params: { image: { image: Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'images', 'mountain.jpg'), 'image/jpeg'), alt: @image.alt, title: @image.title } }
    end
  end

  test 'should update image when logged in' do
    sign_in(users(:john))
    patch admin_image_url(@image), params: { image: { image: Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'images', 'mountain.jpg'), 'image/jpeg'), alt: @image.alt, title: @image.title } }
    assert_redirected_to admin_image_url(@image)
  end

  test 'should not destroy image when not logged in' do
    assert_raises(ActionController::RoutingError) do
      delete admin_image_url(@image)
    end
  end

  test 'should destroy image when logged in' do
    sign_in(users(:john))
    assert_difference('Image.count', -1) do
      delete admin_image_url(@image)
    end

    assert_redirected_to admin_images_url
  end
end
