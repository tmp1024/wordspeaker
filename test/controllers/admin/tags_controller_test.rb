require 'test_helper'

class Admin::TagsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @tag = tags(:one)
  end

  test 'should not get index when logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_tags_url
    end
  end

  test 'should get index when logged in' do
    sign_in(users(:john))
    get admin_tags_url
    assert_response :success
  end

  test 'should not get new when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get new_admin_tag_url
    end
  end

  test 'should get new when logged in' do
    sign_in(users(:john))
    get new_admin_tag_url
    assert_response :success
  end

  test 'should not create tag when not logged in' do
    assert_raises(ActionController::RoutingError) do
      post admin_tags_url, params: { tag: { description: @tag.description, title: @tag.title } }
    end
  end

  test 'should create tag when logged in' do
    sign_in(users(:john))
    assert_difference('Tag.count') do
      post admin_tags_url, params: { tag: { description: @tag.description, title: @tag.title } }
    end

    assert_redirected_to admin_tag_url(Tag.last)
  end

  test 'should not show tag when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_tag_url(@tag)
    end
  end

  test 'should show tag when logged in' do
    sign_in(users(:john))
    get admin_tag_url(@tag)
    assert_response :success
  end

  test 'should not get edit when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get edit_admin_tag_url(@tag)
    end
  end

  test 'should get edit when logged in' do
    sign_in(users(:john))
    get edit_admin_tag_url(@tag)
    assert_response :success
  end

  test 'should not update tag when not logged in' do
    assert_raises(ActionController::RoutingError) do
      patch admin_tag_url(@tag), params: { tag: { description: @tag.description, title: @tag.title } }
    end
  end

  test 'should update tag when logged in' do
    sign_in(users(:john))
    patch admin_tag_url(@tag), params: { tag: { description: @tag.description, title: @tag.title } }
    assert_redirected_to admin_tag_url(@tag)
  end

  test 'should not destroy tag when not logged in' do
    assert_raises(ActionController::RoutingError) do
      delete admin_tag_url(@tag)
    end
  end

  test 'should destroy tag when logged in' do
    sign_in(users(:john))
    assert_difference('Tag.count', -1) do
      delete admin_tag_url(@tag)
    end

    assert_redirected_to admin_tags_url
  end
end
