require 'test_helper'

class Admin::DashboardControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should not get index when not logged in' do
    assert_raises(ActionController::RoutingError) do
      get admin_url
    end
  end

  test 'should get index when logged in' do
    sign_in(users(:john))
    get admin_url
    assert_response :success
  end
end
