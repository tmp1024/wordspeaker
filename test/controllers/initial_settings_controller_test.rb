require 'test_helper'

class InitialSettingsControllerTest < ActionDispatch::IntegrationTest
  test 'should not get index when user is not the target' do
    get init_index_url
    assert_response :found
  end

  test 'should get index' do
    Setting.delete_all
    get init_index_url
    assert_response :success
  end

  test 'should not get setting when user is not the target' do
    get init_setting_url
    assert_response :found
  end

  test 'should get setting' do
    Setting.delete_all
    get init_setting_url
    assert_response :success
  end

  test 'should not create setting when user is not the target' do
    assert_no_difference('Setting.count') do
      post init_setting_url, params: { setting: { site_name: 'wordspeaker', site_description: '', syntax_highlight: 'base16 light', theme: 'default' } }
    end
    assert_response :found
  end

  test 'should create setting' do
    Setting.delete_all
    assert_difference('Setting.count') do
      post init_setting_url, params: { setting: { site_name: 'wordspeaker', site_description: '', syntax_highlight: 'base16 light', theme: 'default' } }
    end
    assert_redirected_to init_completion_url
  end

  test 'should get completion' do
    get init_completion_url
    assert_response :success
  end
end
