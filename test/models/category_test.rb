require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  test 'Should not save category without title' do
    category = Category.new
    assert_not category.save
  end

  test 'Should save category' do
    category = Category.new(title: 'New category')
    assert category.save
  end
end
