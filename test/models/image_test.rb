require 'test_helper'

class ImageTest < ActiveSupport::TestCase
  setup do
    @image = images(:one)
  end

  test 'should not save image without image' do
    image = Image.new(alt: 'image alt', title: 'image title')
    assert_not image.save
  end

  test 'should save image' do
    image = Image.new(image: Rack::Test::UploadedFile.new(Rails.root.join('test', 'fixtures', 'files', 'images', 'mountain.jpg'), 'image/jpeg'), alt: 'image alt', title: 'image title')
    assert image.save
  end
end
