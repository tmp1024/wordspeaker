require 'test_helper'

class SettingTest < ActiveSupport::TestCase
  test 'must have site_name' do
    setting = Setting.new(site_name: nil)
    assert setting.invalid?
    assert_includes setting.errors[:site_name], "can't be blank"
  end

  test 'site_name must be valid' do
    setting = Setting.new(site_name: 'wordspeaker')
    assert setting.valid?
  end

  test 'must have syntax_highlight' do
    setting = Setting.new(site_name: 'wordspeaker', syntax_highlight: nil)
    assert setting.invalid?
    assert_includes setting.errors[:syntax_highlight], "can't be blank"
  end

  test 'syntax_highlight must be valid' do
    setting = Setting.new(site_name: 'wordspeaker', syntax_highlight: 'base16 light')
    assert setting.valid?
  end

  test 'must have theme' do
    setting = Setting.new(site_name: 'wordspeaker', theme: nil)
    assert setting.invalid?
    assert_includes setting.errors[:theme], "can't be blank"
  end

  test 'theme must be valid' do
    setting = Setting.new(site_name: 'wordspeaker', theme: 'default')
    assert setting.valid?
  end
end
