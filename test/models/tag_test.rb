require 'test_helper'

class TagTest < ActiveSupport::TestCase
  test 'Should not save tag without title' do
    tag = Tag.new
    assert_not tag.save
  end

  test 'Should save tag' do
    tag = Tag.new(title: 'New Tag')
    assert tag.save
  end
end
