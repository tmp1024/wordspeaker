require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  setup do
    @article = articles(:one)
  end
  
  test 'should not save article without title' do
    article = Article.new(document_type: Article.document_types[:html])
    assert_not article.save
  end

  test 'should not save article without document_type' do
    article = Article.new(title: 'New title')
    article.document_type = nil
    assert_not article.save
  end

  test 'should not save article without permalink' do
    article = Article.new(title: 'New title', document_type: Article.document_types[:html])
    assert_not article.save
  end

  test 'should save article' do
    article = Article.new(title: 'New article', permalink: 'new-article')
    assert article.save
  end

  test 'should not save when the number of permalink character count is 129 or more' do
    article = Article.new(title: 'New article', permalink: 'new-article')
    str = [*:a..:z, *0..9, '-', '_'].freeze
    article.permalink = Array.new(129) { str.sample }.join
    assert_not article.save
  end

  test 'should save when the number of permalink character count is 128 or less' do
    article = Article.new(title: 'New article', permalink: 'new-article')
    str = [*:a..:z, *0..9, '-', '_'].freeze
    article.permalink = Array.new(128) { str.sample }.join
    assert article.save
  end

  test 'should not get reservation article' do
    @article.publication!
    @article.publication_date = Time.zone.now + 5.minutes
    @article.save
    assert_not Article.only_publication.include? @article
  end
end
