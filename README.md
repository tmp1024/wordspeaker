# wordspeaker
[![pipeline status](https://gitlab.com/tmp1024/wordspeaker/badges/master/pipeline.svg)](https://gitlab.com/tmp1024/wordspeaker/commits/master)
[![coverage report](https://gitlab.com/tmp1024/wordspeaker/badges/master/coverage.svg?job=ruby2.4.3-postgres10-test)](https://gitlab.com/tmp1024/wordspeaker/commits/master)

A simple blog made with Ruby.

## Description
Do not you think the recent blogging system is complicated?

Wordspeaker proposes a blog system with the necessary minimum functionality.

## Requirement
- Ruby
    - 2.5
    - 2.4
    - 2.3
    - 2.2
- Rails
    - 5.1
- Database
    - PostgreSQL
        - 10
        - 9.6
    - MySQL
        - 8.0 (experimental)
        - 5.7
        - 5.6

## Usage
Clone this repository.

Install Ruby interpreter.

Move to repository path and Install gems.

    bundle install

Install frontend packages

    bin/yarn install

Copy config/database.sample.yml as config/database.yml.

Edit config/database.yml to configure the database.

Migrate the database.

    rails db:migrate

Insert seed data.

    rails db:seed

Start the server.

    rails server

Let's get started.

## Author
[tmp1024](https://gitlab.com/tmp1024)

## LICENSE
[MIT](https://gitlab.com/tmp1024/wordspeaker/blob/master/LICENSE)
